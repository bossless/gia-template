# Using the gia interactive template

This project assumses you have installed Node, Grunt and necessary files.

## Project structure

Below is the strcuture inside the project template. Edit the corresponding files as needed.
```
project-folder
~~~~~~

.. Gruntfile.js					<< EDIT: your project path prior to deployment
.. package.json
.. /src
   ~~~~~~
   .. codeobject.html			<< EDIT: your html inside the <figure> tag
   .. asset.html
   .. index.html
   .. boot.js					<< EDIT: paths to libraries you use
   .. /css
      main.scss					<< EDIT: your css
   .. /js
       main.js					<< EDIT: references to js libraries, your code
	   .. /utils
	       curl.js
	   .. /lib
	       d3.js
		   jquery.js
```

# Initialize Grunt
This loads the necssary packages for Grunt to run. You only need to run this if you are working on a project for the first time or have just checked out an existing project.

Navigate to the project folder in terminal and run:

```
npm install
```

# Preview a project

## Generate a preview
Generate a preview by running the command below. The preview will not update as you edit the src directory.

```
grunt preview
```

## Build preview and view using your local webserver. 
Manually reload browser to view changes.

```
grunt
```  

## Build preview using Grunt server instead of the local webserver. 
Browser will reload with updates.

```
grunt live
```

# Deploy a project
 
##1. Set the project path
Create the asset in r2 and stomp the url using the redirect tool so that it includes 'ng-interactive' in the live url. In order for a project o show up on the mobile site, it must contain 'ng-interactive'. 

Files are then uploaded to s3 following the path after theguardian.com in the url. 

Convention:

    Live r2 url: http://www.theguardian.com/[path]

    s3 project path: next-gen/[path]

    * Note the s3 path must go in the root directory next-gen

See example below: 

    Live r2 url: http://www.theguardian.com/media/ng-interactive/2013/sep/02/media-100-2013-full-list

    Project path on s3: next-gen/media/ng-interactive/2013/sep/02/media-100-2013-full-list

### Edit the projectpath parameter in the project's Gruntfile.js

##2. Deploy to s3

```
grunt deploy
```
Note: this assumes you've added your s3 keys to your ./base_profile. See template Readdme to set this up.

