/*global module:false*/
module.exports = function(grunt) {

	'use strict';

    //Create some functions for replacing tags in documents
    var makeTagReplacer = function( tags ) {
            
            var replacer = function ( content, srcpath ) {
                    var tagName, tagValue, regex;

                    for ( tagName in tags ) {
                            tagValue = grunt.config.process( tags[ tagName ] );
                            regex = new RegExp( '<%=\\s*' + tagName + '\\s*%>', 'g' );
                            
                            content = content.replace(regex, tagValue);
                    }

                    return content;
            };

            return replacer;
    };

    var tagReplacer = makeTagReplacer({
            codeobject: grunt.file.read( 'src/codeobject.html' ),
			outputDir: '<%= outputDir %>',
			projectUrl: '<%= isProd ? baseUrl + projectPath : "" %>',
    });


	grunt.initConfig({
		
		//config paths
		isProd: grunt.option( 'isProd' ),
		
		builds : {
			preview : 'build/preview',
			prod : 'build/prod',
			zip: 'build/.tmp'
		},
		outputDir: '<%= isProd ? builds.prod : builds.preview %>',
		

		baseUrl: 'http://interactive.guim.co.uk/',
        projectPath: '', // format: 'next-gen/world/ng-interactive/...'
		
		
		watch: {
			sass: {
				files: 'src/**/*.scss',
				tasks: 'sass'
			},
			js: {
				files: 'src/js/**',
				tasks: 'copy:js'
			},
			src: {
				files: ['src/**', '!src/js/**/*'],
				tasks: 'copy:files'
			},
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>',
					interrupt: true
                },
                files: [
                    '<%= outputDir %>/*.html',
					'<%= outputDir %>/css/{,*/}*.css',
					'<%= outputDir %>/js/{,*/}*.js',
                ]
            }
		},
		
        connect: {
            options: {
                port: 9000,
                livereload: 35729,
                // change this to '0.0.0.0' to access the server from outside
                hostname: 'localhost'
            },
            livereload: {
                options: {
                    open: true,
                    base: '<%= outputDir %>' 
                }
            }
        },
		
		clean: {
                build: [ '<%= outputDir %>' ],
				zip: [ '<%= builds.zip %>' ]
        },
		
		jshint: {
                files: [
                        'src/js/**/*.js', 
                        '!src/js/boot.js', //exclude
						'!src/js/lib/**/*.js', //exclude
						'!src/js/utils/**/*.js' //exclude'
                ],
                options: {
                        undef: true,
                        unused: true,
                        boss: true,
                        smarttabs: true,
                        globals: {
                                define: true,
                                window: true,
                                document: true,
                                navigator: true,
                                XMLHttpRequest: true,
                                setTimeout: true,
                                clearTimeout: true,
                                Image: true
                        },
                        // don't actually fail the build
                        force: true
                }
        },
	    // Copy files
		copy: {
		       files: {
	               files: [{
	                       expand: true,
	                       cwd: 'src',
	                       src: [ '**', '!js/**/*', '!codeobject.html','!css/**/*' ],
	                       dest: '<%= outputDir %>'
	               }],
				   options: {
					   processContent: tagReplacer
                   }
		       },
		       js: {
	               files: [{
	                       expand: true,
	                       cwd: 'src/js',
	                       src: [ '**' ],
	                       dest: '<%= outputDir %>/js' // tmp, as it needs to be optimised
	               }],
	               options: {
   	                       processContent: tagReplacer
   	               }
		       },
		       zip: {
	               files: [{
	                       expand: true,
	                       cwd: '<%= outputDir %>',
	                       src: [ '**' ],
	                       dest: '<%= builds.zip %>'
	               }],
				   options: {
					   processContent: tagReplacer
                   }
		       },
		},
		//Compile .scss files
        sass: {
            main: {
                files: [{
                        src: 'src/css/main.scss',
                        dest: '<%= outputDir %>/css/main.css'
                }],
                options: {
                        style: ( '<%= isProd ? "compressed" : "expanded" %>' )
                }
            }
        },
		compress: {
			main: {
				options: {
					mode: 'gzip'
				},				
	
				files: [
					{expand: true, cwd: '<%= builds.zip %>', src: ['**/*.js'], dest: '<%= outputDir %>', ext: '.js', filter: 'isFile'},
					{expand: true, cwd: '<%= builds.zip %>',  src: ['**/*.css'], dest: '<%= outputDir %>', ext: '.css', filter: 'isFile'}
				]
			}
		},
		aws_s3: {
			options: {
				accessKeyId: '<%= process.env["AWS_ACCESS_KEY_ID"] %>', // Use the variables
				secretAccessKey: '<%= process.env["AWS_SECRET_ACCESS_KEY"] %>', // You can also use env variables
			    uploadConcurrency: 5, // 5 simultaneous uploads
			    downloadConcurrency: 5, // 5 simultaneous downloads
				params: {
					CacheControl: 'max-age=30'
				}
			},
			prod: {
				options: {
					bucket: 'gdn-cdn',
					differential: true // Only uploads the files that have changed
			    },
			    files: [
					{expand: true, cwd: '<%= outputDir %>', src: ['**', '!css/**/*','!js/**/*'], dest: '<%= projectPath %>'},
					{expand: true, cwd: '<%= outputDir %>', src: ['css/**/*'], dest: '<%= projectPath %>', params: { ContentType: 'text/css', ContentEncoding: 'gzip' } },
					{expand: true, cwd: '<%= outputDir %>', src: ['js/**/*'], dest: '<%= projectPath %>', params: { ContentType: 'text/javascript', ContentEncoding: 'gzip' } },

			    ]
			}
			
		}


	});


	// These plugins provide necessary tasks.
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-compress');
	grunt.loadNpmTasks('grunt-aws-s3');
	
	
	grunt.registerTask( 'setPrev', function(){
		grunt.config( 'isProd', false );
	});
	
	grunt.registerTask( 'setProd', function(){
		grunt.config( 'isProd', true );
	});

	var buildSequence = [
			'clean:build',
			'jshint',
			'copy:js',
			'sass',
			'copy:files'
	];
	
	grunt.registerTask( 'preview',['setPrev'].concat( buildSequence) );
    grunt.registerTask( 'prod', [ 'setProd' ].concat( buildSequence ) );
	
	
    grunt.registerTask('live', [
        'preview',
        'connect:livereload',
		'watch'
    ]);
	
	grunt.registerTask( 'deploy', [
		'prod',
		'copy:zip',
		'compress', 
		'clean:zip',
		'aws_s3:prod'
	]);

	// default task - generate dev build and watch for changes
	grunt.registerTask( 'default', [
		'preview',
		'watch'
	]);

};