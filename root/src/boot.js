define([],function () {

        'use strict';
  		/**
		 *
		 * @param el        : The Element of the interactive that is being progressively enhanced.
		 * @param context   : The DOM context this module must work within.
		 * @param config    : The configuration object for this page.
		 * @param mediator  : The event system (publish/subscribe) for this page.
		 *
		 **/

        return {
                boot: function ( el, context, config, mediator ) {
				        var config = {
				                context: 'interactive', 
				                baseUrl: '<%= projectUrl %>js',
								paths: {
									//path does not include js/
									// inlcude the reference name as a dependency in main.js
									main: 'main',
									d3: 'lib/d3.v3.min',
									jquery: 'lib/jquery-2.0.3.min'
								}
				        };

                        // determine whether we're using requirejs (i.e. we're on R2) or curl
                        if ( typeof require() === 'function' ) {
                                // requirejs, i.e. R2
                                var req = require.config( config );
                                req([ 'main' ], function( app ){
                                	app.launch(el)
                                });
                        } else {
                                // curl, i.e. next-gen
                                require( config, [ 'main' ] ).then( function( app ){
                                	app.launch(el)
                                } );
                        }
                }
        };

});