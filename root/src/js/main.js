define(['d3', 'jquery'],  //reference dependency names from boot.js file
function ( d3, $ ) {

	'use strict';

	 var gia = {
		 launch: function ( el ) {
			 document.getElementById('interactiveLog').innerHTML = "Launched."
		 }
	 };

	 window.gia = gia; // useful for debugging!

	 return gia;

});