# Setup the environment

### 1. Install [node](http://nodejs.org)
Do this in terminal if you have brew installed:

```
brew install node
```

### 2. Install [grunt-cli](http://gruntjs.com/getting-started)
Do this by running this command in terminal:
```
npm install -g grunt-cli
```


## Install the Grunt framework and interactive template

### 3. Install grunt-init
Download the Grunt template framework

```
sudo npm install -g grunt-init
```

### 4. Install gia interactive template
Download the interactive template. Replace the [USERNAME] with your bitbucket username and run the following command in terminal.

```
git clone https://[USERNAME]@bitbucket.org/gia_src/gia-template.git ~/.grunt-init/gia
```

# Store your s3 keys

You need to store your s3 keys as variables in your terminal profile in order to deploy.

```
sudo nano ~/.bash_profile
```

Add these variables to your profile replacing [key] and [secret] with actual values

```
export AWS_ACCESS_KEY_ID=[key]
export AWS_SECRET_ACCESS_KEY=[secret]
```

Then hit control X and save changes.


# Create Project

Create the folder where you want to build the project and run the following command to setup the project template

```
grunt-init gia
```

the follow the instructions in the project readme.md file