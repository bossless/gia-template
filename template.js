/*
	grunt-init-gia
*/


(function ( exports, child_process ) {

	'use strict';

	// Basic template description.
	exports.description = 'Create a new Guardian US Interactive.';

	// Template-specific notes to be displayed before question prompts.
	exports.after = 'Now, install project dependencies with _npm install_ (or _npm install --no-registry_ to install modules from cache, which may be much faster). This will download grunt and the plugins this project uses. For further instructions do _cat README.md_';

	// Any existing file or directory matching this wildcard will cause a warning.
	exports.warnOn = '*';

	// The actual init template.
	exports.template = function(grunt, init, done) {

		var options, prompts, complete, getDefaultPath;

		options = {};
		prompts = [
			init.prompt( 'name' )
		];

		complete = function ( err, props ) {
			var files;

			props.Name = props.name.substr( 0, 1 ).toLowerCase() + props.name.substring( 1 );

			files = init.filesToCopy( props );

			init.copyAndProcess( files, props );
			done();
		};

		init.process( options, prompts, complete );

	};

}( exports, require( 'child_process' ) ) );